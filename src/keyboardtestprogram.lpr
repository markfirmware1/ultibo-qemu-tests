program KeyboardTestProgram;

{$mode objfpc}{$H+}

{ QEMU VersatilePB Application                                                 }
{  Add your program code below, add additional units to the "uses" section if  }
{  required and create new units by selecting File, New Unit from the menu.    }
{                                                                              }
{  To compile your program select Run, Compile (or Run, Build) from the menu.  }

uses
  QEMUVersatilePB,
  GlobalConfig,
  GlobalConst,
  GlobalTypes,
  Platform,
  Threads,
  SysUtils,
  Classes,
  Ultibo,
  VersatilePb,
  Console,
  Network,
  Winsock2
  { Add additional units here };

var
 Window:TWindowHandle;

procedure SerialMessage(Message:String);
var
 C:Char;
 S:String;
 procedure SerialByte(C:Char);
 begin
  PLongWord(VERSATILEPB_UART0_REGS_BASE)^ := Ord(C);
 end;
begin
 S:=Format('%08.3f %s',[ClockGetTotal / (1000 * 1000), Message]);
 WriteLn(S);
 for C in S do
  SerialByte(C);
 SerialByte(Char(13));
 SerialByte(Char(10));
end;

var
 IpAddress:String;

procedure DetermineIpAddress;
var
 Winsock2TCPClient:TWinsock2TCPClient;
begin
 SerialMessage('Obtaining IP address ...');
 Winsock2TCPClient:=TWinsock2TCPClient.Create;
 IpAddress:=Winsock2TCPClient.LocalAddress;
 while (IpAddress = '') or (IpAddress = '0.0.0.0') or (IpAddress = '255.255.255.255') do
  begin
   Sleep(100);
   IpAddress:=Winsock2TCPClient.LocalAddress;
  end;
 Winsock2TCPClient.Free;
 SerialMessage(Format('IP address %s',[IpAddress]));
end;

procedure Main;
begin
 Window:=ConsoleWindowCreate(ConsoleDeviceGetDefault,CONSOLE_POSITION_FULL,True);
 ConsoleWindowSetBackColor(Window,COLOR_BLACK);
 ConsoleWindowSetForeColor(Window,COLOR_WHITE);
 ConsoleClrScr;
 SerialMessage('program start');
 DetermineIpAddress;
end;

begin
 try
  try
   Main;
  except on E:Exception do
   SerialMessage(Format('Exception %s',[E.Message]));
  end;
 finally
  SerialMessage('program stop');
 end;
end.
