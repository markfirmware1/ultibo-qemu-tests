#!/bin/bash

exec {STDOUT}>&1 > pre-report.txt
function finish {
    local EXITCODE=$1
    shift
    local MESSAGE="$*"

    echo
    echo $MESSAGE
    exec >&- >&$STDOUT

    kill -9 $QEMUPID > /dev/null 2>&1

    sed 's/\x1B\[K//g' pre-report.txt | sed 's/^(qemu) .*\x1B\[D//' > test-report.txt
    rm pre-report.txt
    unix2dos test-report.txt 2> /dev/null

    echo see test-report.txt
    exit $EXITCODE
}

date

KERNEL=src/kernel.bin

function show {
    echo $*
    eval "$*"
}

function monitor {
    echo $* >&$MONITOR
}

function drain {
    local FD=$1
    shift
    echo
    echo $* ...
    while read -t 2 line <&$FD
    do
        echo $line
    done
}

function drainuntil {
    local FD=$1
    shift
    local KEYWORD=$1
    shift
    local TIMELIMIT=60
    echo
    echo $* ...
    while true
    do
	read -t $TIMELIMIT line <&$FD
        if [[ $? != 0 ]]
        then
	    finish 1 error - did not finish - $TIMELIMIT seconds without serial output
        fi
        echo $line
        if [[ $line =~ $KEYWORD ]]
        then
            break
        fi
    done
}

function delay {
    local SECONDS=$1
    shift
    echo $* ...
    sleep $SECONDS
}

echo
echo compiling ...
cat src/keyboardtestprogram.lpr
/c/Ultibo/Core/lazbuild.exe -B --lazarusdir=/c/Ultibo/Core src/keyboardtestprogram.lpi > build.log
cat build.log |
    egrep -i 'error|warning|hint|note' | \
    egrep -iv '1022|11030|11031|hint.*runtool' > errors-build.txt
grep . errors-build.txt > /dev/null
if [[ $? == 0 ]]
then
    echo
    cat errors-build.txt
    echo
    finish 1 build failure
fi

echo
echo sha1 ...
sha1sum $KERNEL
sha1sum src/keyboardtestprogram.lp[ir]

echo
echo qemu ...
/c/Ultibo/Core/qemu/qemu-system-arm \
    -M versatilepb \
    -cpu cortex-a8 \
    -m 256M \
    -display none \
    -net dump \
    -net nic \
    -net user \
    -kernel $KERNEL \
    -monitor tcp::39004,server,nodelay \
    -serial  tcp::39000,server,nodelay \
    -serial  tcp::39001,server,nodelay \
    -serial  tcp::39002,server,nodelay \
    -serial  tcp::39003,server,nodelay \
    2> qemu.stderr \
    &
QEMUPID=$!
echo connecting ...
exec {MONITOR}<>/dev/tcp/localhost/39004
exec {SERIAL0}<>/dev/tcp/localhost/39000
exec {SERIAL1}<>/dev/tcp/localhost/39001
exec {SERIAL2}<>/dev/tcp/localhost/39002
exec {SERIAL3}<>/dev/tcp/localhost/39003

delay 4 running
drainuntil $SERIAL0 "program stop" serial0
sleep 1
monitor screendump screendump.ppm
monitor quit
drain $MONITOR qemu monitor
wait
echo

echo qemu.stderr ...
cat qemu.stderr
#show egrep -iv 'alsa' qemu.stderr

echo
magick screendump.ppm screendump.png
file screendump.png

echo
echo tools ...
/c/Ultibo/Core/qemu/qemu-system-arm --version
sha1sum /c/Ultibo/Core/fpc/3.1.1/bin/i386-win32/fpc.exe
sha1sum /c/Ultibo/Core/qemu/qemu-system-arm.exe

echo
echo 'Core (rtl and packages) ...'
grep -i ultibo_release /c/Ultibo/Core/fpc/3.1.1/source/rtl/ultibo/core/globalconst.pas

echo
find /c/Ultibo/Core/fpc/3.1.1/source/rtl/ultibo/ /c/Ultibo/Core/fpc/3.1.1/source/packages/ultibounits/ -type f -exec sha1sum '{}' ';'

finish 0 succeeded
